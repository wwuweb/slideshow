<?php
/**
 * @file
 * slideshow.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function slideshow_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function slideshow_node_info() {
  $items = array(
    'slideshow' => array(
      'name' => t('Slideshow'),
      'base' => 'node_content',
      'description' => t('Create slideshows using this content type.'),
      'has_title' => '1',
      'title_label' => t('Slideshow Title'),
      'help' => '',
    ),
  );
  return $items;
}
