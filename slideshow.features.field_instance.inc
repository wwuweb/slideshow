<?php
/**
 * @file
 * slideshow.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function slideshow_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-slideshow-field_slideshow_images'
  $field_instances['node-slideshow-field_slideshow_images'] = array(
    'bundle' => 'slideshow',
    'deleted' => 0,
    'description' => 'Upload each photo for the slideshow here. Drag images around to set the order.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'flexslider_fields',
        'settings' => array(
          'caption' => 1,
          'optionset' => 'default',
        ),
        'type' => 'flexslider',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_slideshow_images',
    'label' => 'Slideshow Images',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => 'slideshows',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'icon_link' => 0,
          'image' => 0,
          'image_flexslider_full' => 0,
          'image_flexslider_thumbnail' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_square_thumbnail' => 0,
          'image_thumbnail' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Slideshow Images');
  t('Upload each photo for the slideshow here. Drag images around to set the order.');

  return $field_instances;
}
